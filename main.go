package main

import (
	"time"
	"fmt"
	"math/rand"
)

type Event struct{
	Data int
}

func EmitEvent() *Event {
	time.Sleep(5 * time.Second)

	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	return &Event{
		Data: r1.Intn(100),
	}
}

func BroadCaster(ev *Event, receiver chan int){
	for{
		time.Sleep(6 * time.Second)
		receiver <- ev.Data
	}
}

func HandleEvent(val int){
	fmt.Println(val)
}

func Targets(ev *Event, ch chan int){
	go BroadCaster(ev ,ch)

	for{
		select {
		case val := <- ch:
			HandleEvent(val)
		default:
			time.Sleep(1 * time.Second)
			fmt.Println("Waiting for the event!")
		}
	}
}

func main(){
	var ev *Event
	var receivers []chan int

	receivers = append(receivers, make(chan int))
	receivers = append(receivers, make(chan int))

	for{
		ev = EmitEvent()

		for i := range receivers{
			go Targets(ev, receivers[i])
		}
	}

	fmt.Scanln()
}